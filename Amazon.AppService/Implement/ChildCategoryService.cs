﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.AppService.Interface;
using Amazon.Models;
using System.Data.Entity;
using Amazon.Models.DataModel;
using Amazon.UOW.Interface;

namespace Amazon.AppService.Implement
{
    public class ChildCategoryService : IChildCategoryService
    {
        public ApplicationDbContext DbContext = new ApplicationDbContext();
        public IUnitOfWork _unitOfWork;

        public ChildCategoryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void CreateChildCategory(ChildCategory childCategory)
        {
            _unitOfWork.ChildCategoryRepository.Create(childCategory);
        }

        public void DeleteChildCategory(Guid idCategory)
        {
            throw new NotImplementedException();
        }

        public List<ChildCategory> GetAllChildCategory()
        {
            var listChildCategory = _unitOfWork.ChildCategoryRepository.GetAll().Include(n=>n.Category).ToList();
            return listChildCategory;
        }

        public ChildCategory GetChildCategoryByIdCategory(Guid idCategory)
        {
            throw new NotImplementedException();
        }

        public void UpdateChildCategory(ChildCategory childCategory)
        {
            throw new NotImplementedException();
        }
    }
}
