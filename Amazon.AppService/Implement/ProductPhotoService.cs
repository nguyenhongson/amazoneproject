﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.AppService.Interface;
using Amazon.AppService.ViewModel;
using Amazon.Models;
using Amazon.Models.DataModel;
using Amazon.UOW.Interface;

namespace Amazon.AppService.Implement
{
    public class ProductPhotoService : IProductPhotoService
    {
        public IUnitOfWork _unitOfWork;
        public ApplicationDbContext DbContext = new ApplicationDbContext();
        public ProductPhotoService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void CreateProductPhoto(ProductPhoto productPhoto)
        {
            _unitOfWork.ProductPhotoRepository.Create(productPhoto);
        }

        public void DeleteProductPhoto(Guid idProductPhoto)
        {
            throw new NotImplementedException();
        }

        public List<ProductPhotoViewModel> GetListPhotoByIdProduct(Guid ProductID)
        {
            //get list photo of product
            List<ProductPhotoViewModel> listViewModel = new List<ProductPhotoViewModel>();
            List<ProductPhoto>  listProductPhoto= (from a in DbContext.ProductPhoto where a.ProductID == ProductID select a).ToList();
            foreach(var item in listProductPhoto)
            {
                ProductPhotoViewModel onePhoto = new ProductPhotoViewModel();
                onePhoto.PhotoPath = item.PhotoPath;
                onePhoto.ProductID = item.ProductID;
                onePhoto.Status = item.Status;
                onePhoto.PoductPhotoID = item.PoductPhotoID;
                listViewModel.Add(onePhoto);
            }
            return listViewModel;
        }

        public void UpdateProductPhoto(ProductPhoto productPhoto)
        {
            throw new NotImplementedException();
        }
    }
}
