﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Amazon.AppService.Interface;
using Amazon.Models;
using Amazon.Models.DataModel;

namespace Amazon.AppService.Implement
{
    public class ProductPropertyService : IProductPropertyService
    {
        public ApplicationDbContext DbContext = new ApplicationDbContext();
        public void CreateProductProperty(ProductProperty productProperty)
        {
            throw new NotImplementedException();
        }
       
        public void DeleteProductProperty(Guid idProductProperty)
        {
            throw new NotImplementedException();
        }

        public ProductProperty GetProductPropertyByIdProduct(Guid idProductProperty)
        {
            throw new NotImplementedException();
        }

        public void UpdateProductProperty(ProductProperty productProperty)
        {
            throw new NotImplementedException();
        }
        static public void Serialize(List<string> details) 
        {
            SerializableProductProperty ProductObj = new SerializableProductProperty();

            // Set some dummy values
            ProductObj.ProductID = "ProductIdNumberone";
            ProductObj.ProductPropertyName = "A";
            ProductObj.ProductPropertyValue = "B";

            // Create a new XmlSerializer instance with the type of the test class
            XmlSerializer SerializerObj = new XmlSerializer(typeof(SerializableProductProperty));

            // Create a new file stream to write the serialized object to a file
            TextWriter WriteFileStream = new StreamWriter(@"F:\fileXml.xml");
            SerializerObj.Serialize(WriteFileStream, ProductObj);
            WriteFileStream.Close();
        }

        public List<SerializableProductProperty> GetlistPropertyByIdProduct(Guid? ProductID)
        {
           // List<ProductProperty> listpro = new List<ProductProperty>();

            List<SerializableProductProperty> listproper = new List<SerializableProductProperty>();

            //get list property by idproduct
            var listproperty = from a in DbContext.ProductProperty where a.ProductID == ProductID select a.ProprertyXML;
            foreach(var item in listproperty)
            {
                string oneproperty = item;
                XmlSerializer serializer = new XmlSerializer(typeof(SerializableProductProperty));
                using (TextReader reader = new StringReader(oneproperty))
                {
                    SerializableProductProperty result = (SerializableProductProperty)serializer.Deserialize(reader);
                    listproper.Add(result);
                }
            }
            return listproper;
        }
        protected SerializableProductProperty FromXml<SerializableProductProperty>(String xml)
        {
            SerializableProductProperty returnedXmlClass = default(SerializableProductProperty);

            try
            {
                using (TextReader reader = new StringReader(xml))
                {
                    try
                    {
                        returnedXmlClass =
                            (SerializableProductProperty)new XmlSerializer(typeof(SerializableProductProperty)).Deserialize(reader);
                        
                    }
                    catch (InvalidOperationException)
                    {
                        // String passed is not XML, simply return defaultXmlClass
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return returnedXmlClass;
        }









        [Serializable()]
        public class SerializableProductProperty
        {
            private string productID;
            public string ProductID
            {
                get { return productID; }
                set { productID = value; }
            }
            private string productPropertyName;
            public string ProductPropertyName
            {
                get { return productPropertyName; }
                set { productPropertyName = value; }
            }
            private string productPropertyValue;
            public string ProductPropertyValue
            {
                get { return productPropertyValue; }
                set { productPropertyValue = value; }
            }
        }

    }
}
