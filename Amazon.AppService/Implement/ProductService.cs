﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using Amazon.AppService.Interface;
using Amazon.AppService.ViewModel;
using Amazon.Models;
using Amazon.Models.DataModel;
using Amazon.UOW.Interface;
using AutoMapper;

namespace Amazon.AppService.Implement
{
    public class ProductService : IProductService
    {
        public ApplicationDbContext DbContext = new ApplicationDbContext();
        public IUnitOfWork _unitOfWork;

        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void CreateProduct(ProductViewModel productViewModel)
        {
            //using DTO
            Product productDB = new Product();
            productDB.ChildCategoryID = productViewModel.ChildCategoryID;
            productDB.ProductID = productViewModel.ProductID;
            productDB.ShopID = productViewModel.ShopID;
            productDB.ProductName = productViewModel.ProductName;
            productDB.ProductAvatar = productViewModel.ProductAvatar;
            productDB.Price = productViewModel.Price;
            productDB.Quantity = productViewModel.Quantity;
            productDB.Description = productViewModel.Description;
            productDB.Status = productViewModel.Status;
            productDB.MinOrder = productViewModel.MinOrder;

            _unitOfWork.ProductRepository.Create(productDB);
        }

        public void CreateProductWithManyProperty(ProductViewModel productViewModel, List<PropertyOfProductXML> propertyXML)
        {
            Product productDB = new Product();
            productDB.ChildCategoryID = productViewModel.ChildCategoryID;
            productDB.ProductID = productViewModel.ProductID;
            productDB.ShopID = productViewModel.ShopID;
            productDB.ProductName = productViewModel.ProductName;
            productDB.ProductAvatar = productViewModel.ProductAvatar;
            productDB.Price = productViewModel.Price;
            productDB.Quantity = productViewModel.Quantity;
            productDB.Description = productViewModel.Description;
            productDB.Status = productViewModel.Status;
            productDB.MinOrder = productViewModel.MinOrder;
            
                 _unitOfWork.ProductRepository.Create(productDB);
            string propertyXMLstring = "";
            foreach (var item in propertyXML)
            {
                SerializableProductProperty ProductObj = new SerializableProductProperty();
                // Set some dummy values
                ProductObj.ProductID = productViewModel.ProductID;
                ProductObj.ProductPropertyName =item.Attribute;
                ProductObj.ProductPropertyValue = item.Value;
                // Create a new XmlSerializer instance with the type of the test class
                XmlSerializer SerializerObj = new XmlSerializer(typeof(SerializableProductProperty));


                // Create a new file stream to write the serialized object to a file
                TextWriter WriteFileStream = new StreamWriter(@"F:\fileXml.xml");
                SerializerObj.Serialize(WriteFileStream, ProductObj);
                WriteFileStream.Close();

                 propertyXMLstring = File.ReadAllText(@"F:\fileXml.xml");
                //then , add record product property

                ProductProperty onePropertyOfProduct = new ProductProperty();
                onePropertyOfProduct.ProductPropertyID = Guid.NewGuid();
                onePropertyOfProduct.ProprertyXML = propertyXMLstring;
                onePropertyOfProduct.ProductID = productViewModel.ProductID;

                //unit of work create property of product
               _unitOfWork.ProductPropertyRepository.Create(onePropertyOfProduct);


              
            }
        }
        public void DeleteProduct(Guid idProduct)
        {
            //code here
            throw new NotImplementedException();
        }
        public List<Product> GetListPoductByIdChildCategory(Guid idChildCategory)
        {
            throw new NotImplementedException();
        }


        public List<Product> GetListProduct()
        {
          // var product= DbContext.Product.Include(p => p.ChildCategory).Include(p => p.Shop).ToList();
            var product = (from a in DbContext.Product
                     join b in DbContext.ChildCategory on a.ChildCategoryID equals b.ChildCategoryID
                     join c in DbContext.Shop on a.ShopID equals c.ShopID
                     select a).ToList();
            int mh = product.Count();
            return product;
            //var product = db.Product.Include(p => p.ChildCategory).Include(p => p.Shop);
            //return View(product.ToList());
        }

        public ProductViewModel GetProduct(Guid ProductID)
        {
            ProductViewModel product = new ProductViewModel();
            var productByID = from a in DbContext.Product
                          join c in DbContext.Shop on a.ShopID equals c.ShopID
                          where a.ProductID == ProductID
                              select new
                          {
                              productName = a.ProductName,
                              productID = a.ProductID,
                              productAvatar = a.ProductAvatar,
                              productDesc=a.Description,
                              productMinOrder=a.MinOrder,
                              productStatus=a.Status,
                              productShopName=c.ShopName,
                              productPrice=a.Price,
                              productQuantity=a.Quantity,
                          };
            foreach (var item in productByID)
            {
                product.ProductName = item.productName;
                product.ProductID = item.productID;
                product.ProductAvatar = item.productAvatar;
                product.Description = item.productDesc;
                product.MinOrder = item.productMinOrder;
                product.Status = item.productStatus;
                product.ShopName = item.productShopName;
                product.Price = item.productPrice;
                product.Quantity = item.productQuantity;
            }
            return product;
        }

        public void UpdateProduct(Product product)
        {
            throw new NotImplementedException();
        }


        [Serializable()]
        public class SerializableProductProperty
        {
            private Guid productID;
            public Guid ProductID
            {
                get { return productID; }
                set { productID = value; }
            }
            private string productPropertyName;
            public string ProductPropertyName
            {
                get { return productPropertyName; }
                set { productPropertyName = value; }
            }
            private string productPropertyValue;
            public string ProductPropertyValue
            {
                get { return productPropertyValue; }
                set { productPropertyValue = value; }
            }
        }
    }
}
