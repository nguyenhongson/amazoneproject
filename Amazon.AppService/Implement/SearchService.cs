﻿using Amazon.AppService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Models;
using Amazon.Models.DataModel;
using Amazon.UOW.Interface;
using Amazon.UOW.Implement;

namespace Amazon.AppService.Implement
{
    public class SearchService : ISearchService
    {
        public IUnitOfWork _unitOfWork;
        ApplicationDbContext db = new ApplicationDbContext();

        public List<Product> SearchProductByCC(string searchString, Guid ChildCategoryID)
        {
            var childCategories = from c in db.ChildCategory select c;

            var _search = from l in db.Product
                          join c in db.ChildCategory on l.ChildCategoryID equals c.ChildCategoryID
                          select new { l.ProductID, l.ProductName, l.Price, l.ProductAvatar, l.ChildCategoryID, c.ChildCategoryName };
            if (!String.IsNullOrEmpty(searchString))
            {
                _search = _search.Where(s => s.ProductName.Contains(searchString));
            }

            if (ChildCategoryID != null)
            {
                _search = _search.Where(x => x.ChildCategoryID == ChildCategoryID);
            }
            List<Product> listProduct = new List<Product>();
            foreach (var item in _search)
            {
                Product temp = new Product();
                temp.ChildCategoryID = item.ChildCategoryID;
                temp.ProductID = item.ProductID;
                temp.ProductAvatar = item.ProductAvatar;
                temp.ProductName = item.ProductName;
                temp.Price = item.Price;
                listProduct.Add(temp);
            }
            return listProduct;
            
        }
    }
}
