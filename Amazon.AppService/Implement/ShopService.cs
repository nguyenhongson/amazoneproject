﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.AppService.Interface;
using Amazon.Models;
using Amazon.Models.DataModel;
using Amazon.UOW.Interface;
using Amazon.Domain.Models;
using Amazon.AppService.ViewModel;
using Microsoft.AspNet.Identity;
using System.Web;

namespace Amazon.AppService.Implement
{
    public class ShopService : IShopService
    {
        ApplicationDbContext db = new ApplicationDbContext();

        private readonly IUnitOfWork _unitOfWork;
        private IShopService _shopService;
        public ShopService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void CreateShop(Shop shop)
        {
            _unitOfWork.ShopRepository.Create(shop);
        }

        public void DeleteShop(Guid idShop)
        {
            Shop shop = db.Shop.Find(idShop);
            db.Shop.Remove(shop);
            db.SaveChanges();
        }

        public Shop GetShopByIdUser(string idUser)
        {
            throw new NotImplementedException();
        }
        public IQueryable<Shop> GetAll()
        {
            var shop = _unitOfWork.ShopRepository.GetAll().Include(m => m.Ward);
            //shop = db.Shop.Include(m => m.Ward);
            return shop;

        }
        public void UpdateShop(Shop shop)
        {
            _unitOfWork.ShopRepository.Update(shop);
        }
        public List<ProductViewModel> GetListProductByShopID(Guid ShopID)
        {
           
            var _shopproduct = from p in db.Product
                          join s in db.Shop on p.ShopID equals s.ShopID
                          join u in db.Users on s.UserID equals u.Id
                          select new { p.ProductID, p.ProductName, p.Price, p.ProductAvatar, p.MinOrder, p.ProductPhotos, p.Quantity, p.Status, p.ProductProperties, p.ProductPromotions, p.ShopID, p.Description };
            if (ShopID != null)
            {
                _shopproduct = _shopproduct.Where(x => x.ShopID == ShopID);
            }
            List<ProductViewModel> ListProductByShopID = new List<ProductViewModel>();
            foreach (var item in _shopproduct)
            {
                ProductViewModel temp = new ProductViewModel();
                temp.ProductID = item.ProductID;
                temp.ProductAvatar = item.ProductAvatar;
                temp.ProductName = item.ProductName;
                temp.Price = item.Price;
                temp.Quantity = item.Quantity;
                temp.Status = item.Status;
                temp.MinOrder = item.MinOrder;
                temp.Description = item.Description;
                ListProductByShopID.Add(temp);
            }
            return ListProductByShopID;
        }

    }
}
