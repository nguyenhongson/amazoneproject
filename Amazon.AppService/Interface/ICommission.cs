﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Models.DataModel;
namespace Amazon.AppService.Interface
{
   public interface ICommission
    {
        void CreateCommission(Commission commission);
        void UpdateCommission(Commission commission);
        void DeleteCommission(Guid idCommission);
    }
}
