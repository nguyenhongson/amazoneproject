﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Models.DataModel;
namespace Amazon.AppService.Interface
{
   public interface IOderService
    {
        void CreateOrder(Order order);
        void UpdateOrder(Order order);


        //get all order
        List<Order> GetAllOrder();

        //get order by id-iduser,idshop,idOrder
        Order GetOrderById(Guid id);




        //not use function delete
        void DeleteOrder(Guid idOrder);
    }
}
