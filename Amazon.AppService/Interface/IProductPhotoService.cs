﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.AppService.ViewModel;
using Amazon.Models.DataModel;
namespace Amazon.AppService.Interface
{
   public interface IProductPhotoService
    {
        void CreateProductPhoto(ProductPhoto productPhoto);
        void UpdateProductPhoto(ProductPhoto productPhoto);
        void DeleteProductPhoto(Guid idProductPhoto);
        List<ProductPhotoViewModel> GetListPhotoByIdProduct(Guid ProductID);
    }
}
