﻿using Amazon.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Amazon.AppService.Implement.ProductPropertyService;

namespace Amazon.AppService.Interface
{
   public interface IProductPropertyService
    {
        void CreateProductProperty(ProductProperty productProperty);
        void UpdateProductProperty(ProductProperty productProperty);
        void DeleteProductProperty(Guid idProductProperty);
        ProductProperty GetProductPropertyByIdProduct(Guid idProductProperty);
        List<SerializableProductProperty> GetlistPropertyByIdProduct(Guid? ProductID);
    }
}
