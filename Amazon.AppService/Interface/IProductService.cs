﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web;
using Amazon.Models.DataModel;
using Amazon.AppService.ViewModel;

namespace Amazon.AppService.Interface
{
    public interface IProductService
    {
        //void CreateProduct(HttpPostedFileBase file,Product product);
        void CreateProduct(ProductViewModel product);
        void CreateProductWithManyProperty(ProductViewModel productViewModel,List<PropertyOfProductXML> propertyXML);
        void DeleteProduct(Guid idProduct);
        void UpdateProduct(Product product);

        List<Product> GetListPoductByIdChildCategory(Guid idChildCategory);
        List<Product> GetListProduct();
        ProductViewModel GetProduct(Guid ProductID);
    }
}
