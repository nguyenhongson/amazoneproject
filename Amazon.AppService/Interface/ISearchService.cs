﻿using Amazon.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.Interface
{
    public interface ISearchService
    {
        List<Product> SearchProductByCC(string searchString, Guid ChildCategoryID);
    }
}
