﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.AppService.ViewModel;
using Amazon.Models.DataModel;
namespace Amazon.AppService.Interface
{
  public  interface IShopService
    {
        IQueryable<Shop> GetAll();
        void CreateShop(Shop shop);
        void UpdateShop(Shop shop);
        void DeleteShop(Guid idShop);
        Shop GetShopByIdUser(string idUser);
        List<ProductViewModel> GetListProductByShopID(Guid ShopID);
    }
}
