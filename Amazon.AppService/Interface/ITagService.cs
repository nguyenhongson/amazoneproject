﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Models.DataModel;
namespace Amazon.AppService.Interface
{
   public interface ITagService
    {
        void CreateTag(Tag tag);
        void UpdateTag(Tag tag);
        void DeleteTag(Guid idTag);
        List<Tag> GetListTagByIdProduct(Guid idProduct);

    }
}
