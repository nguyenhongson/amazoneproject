﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
   public  class ChildCategoryViewModel
    {
     
        public Guid ChildCategoryID { get; set; }
        public Guid CategoryID { get; set; }
        public string ChildCategoryName { get; set; }
        public string ChildCategoryAvatar { get; set; }

        public string CategoryName { get; set; }

    }
}
