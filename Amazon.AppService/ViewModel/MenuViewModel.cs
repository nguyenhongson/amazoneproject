﻿using Amazon.Models;
using Amazon.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
    public class MenuViewModel
    {
        public static List<CategoryViewModel> GetCategoryViews()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var listCategory = from a in db.Category select a;


            List<CategoryViewModel> listCategoryViewModel = new List<CategoryViewModel>();
            foreach (var item in listCategory)
            {
                CategoryViewModel _category = new CategoryViewModel();
                _category.CategoryName = item.CategoryName;
                _category.CategoryID = item.CategoryID;
                listCategoryViewModel.Add(_category);
            }
            return listCategoryViewModel;


        }
        public static List<ChildCategoryViewModel> GetChildCategoryViews()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var listChildCategory = from a in db.ChildCategory
                               join b in db.Category on a.CategoryID equals b.CategoryID
                               select new
                               {
                                   childCategoryName = a.ChildCategoryName,
                                   categoryName = b.CategoryName,
                                   categoryID = b.CategoryID
                               };
            List<ChildCategoryViewModel> listChildCategoryViewModel = new List<ChildCategoryViewModel>();
            foreach (var item in listChildCategory)
            {
                ChildCategoryViewModel _childCategory = new ChildCategoryViewModel();
                _childCategory.ChildCategoryName = item.childCategoryName;
                _childCategory.CategoryName = item.categoryName;
                _childCategory.CategoryID = item.categoryID;
                listChildCategoryViewModel.Add(_childCategory);
            }
            return listChildCategoryViewModel;


        }

    }
}
