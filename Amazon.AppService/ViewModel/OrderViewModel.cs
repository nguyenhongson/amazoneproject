﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
   public class OrderViewModel
    {
        public Guid OrderID { get; set; }
        public float TotalMoney { get; set; }
        public DateTime CreateAt { get; set; }
        public string UserId { get; set; }
        public int Status { get; set; }
        public DateTime TimeUpdate { get; set; }

        public string UserFullName { get; set; }
    }
}
