﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
   public class PaymentViewModel
    {
        public Guid PaymentID { get; set; }
        public string PaymentName { get; set; }
        public string PaymentGuide { get; set; }
        public string PaymentAvatar { get; set; }
        public int Status { get; set; }
    }
}
