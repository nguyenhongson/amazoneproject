﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
   public class ProductPromotionViewModel
    {
        public Guid ProductPromotionID { get; set; }
        public Guid ProductID { get; set; }
        public string ContentPromotion { get; set; }

        public string ProductName { get; set; }
    }
}
