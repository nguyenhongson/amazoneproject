﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
  public  class ProductPropertyViewModel
    {
        public Guid ProductPropertyID { get; set; }
        public Guid ProductID { get; set; }
        public string ProprertyXML { get; set; }

        public string ProductName { get; set; }
    }
}
