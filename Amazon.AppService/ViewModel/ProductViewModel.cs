﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Amazon.AppService.Implement.ProductPropertyService;

namespace Amazon.AppService.ViewModel
{
   public class ProductViewModel
    {
        public Guid ProductID { get; set; }
        public string ProductName { get; set; }
        public Guid ShopID { get; set; }
        public int Quantity { get; set; }
        public string ProductAvatar { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public int MinOrder { get; set; }
        public int Price { get; set; }
        public Guid ChildCategoryID { get; set; }

        public string ShopName { get; set; }
        public string ChildCategoryName { get; set; }
        
    }
    public class ProductViewModelList
    {
       public List<ProductViewModel> listPro { get; set; }
      public  ProductViewModel oneProduct { get; set; }
        public List<SerializableProductProperty> listProperty { get; set; }
        public List<ProductPhotoViewModel> listPhoto { get; set; }
    }
}
