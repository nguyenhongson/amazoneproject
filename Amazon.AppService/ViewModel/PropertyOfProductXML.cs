﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
   public class PropertyOfProductXML
    {
        public string Attribute { get; set; }
        public string Value { get; set; }
    }
}
