﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
   public class ShopViewModel
    {
        public Guid ShopID { get; set; }
        public Guid UserID { get; set; }
        public string ShopName { get; set; }
        public int WardID { get; set; }
        public int Status { get; set; }

        public string UserFullName { get; set; }

    }
}
