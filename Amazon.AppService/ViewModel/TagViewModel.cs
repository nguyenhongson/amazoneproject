﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
   public class TagViewModel
    {
        public Guid TagID { get; set; }
        public Guid CategoryID { get; set; }
        public Guid ChildCategoryID { get; set; }
        public string TagName { get; set; }
        
        public string ChildCategoryName { get; set; }
    }
}
