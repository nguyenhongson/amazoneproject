namespace Amazon.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDataAmazon : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryID = c.Guid(nullable: false),
                        CategoryName = c.String(nullable: false),
                        CategoryAvatar = c.String(),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.ChildCategories",
                c => new
                    {
                        ChildCategoryID = c.Guid(nullable: false),
                        CategoryID = c.Guid(nullable: false),
                        ChildCategoryName = c.String(nullable: false),
                        ChildCategoryAvatar = c.String(),
                    })
                .PrimaryKey(t => t.ChildCategoryID)
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductID = c.Guid(nullable: false),
                        ProductName = c.String(),
                        ShopID = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        ProductAvatar = c.String(),
                        Description = c.String(),
                        Status = c.Int(nullable: false),
                        MinOrder = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        ChildCategoryID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ProductID)
                .ForeignKey("dbo.ChildCategories", t => t.ChildCategoryID, cascadeDelete: true)
                .ForeignKey("dbo.Shops", t => t.ShopID, cascadeDelete: true)
                .Index(t => t.ShopID)
                .Index(t => t.ChildCategoryID);
            
            CreateTable(
                "dbo.ProductColors",
                c => new
                    {
                        ProductColorID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        ColorName = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.ProductColorID)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.ProductPhotoes",
                c => new
                    {
                        PoductPhotoID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        PhotoPath = c.String(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PoductPhotoID)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.ProductPromotions",
                c => new
                    {
                        ProductPromotionID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        ContentPromotion = c.String(),
                    })
                .PrimaryKey(t => t.ProductPromotionID)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.Shops",
                c => new
                    {
                        ShopID = c.Guid(nullable: false),
                        UserID = c.Guid(nullable: false),
                        ShopName = c.String(maxLength: 300),
                        WardID = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ShopID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .ForeignKey("dbo.Wards", t => t.WardID, cascadeDelete: true)
                .Index(t => t.WardID)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Commissions",
                c => new
                    {
                        CommissionID = c.Guid(nullable: false),
                        ShopID = c.Guid(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        PayDate = c.DateTime(nullable: false),
                        TotalMoney = c.Single(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommissionID)
                .ForeignKey("dbo.Shops", t => t.ShopID, cascadeDelete: true)
                .Index(t => t.ShopID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderID = c.Guid(nullable: false),
                        TotalMoney = c.Single(nullable: false),
                        CreateAt = c.DateTime(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Status = c.Int(nullable: false),
                        TimeUpdate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.OrderID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        OrderDetailID = c.Guid(nullable: false),
                        OrderID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        Price = c.Single(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        TimeUpdate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.OrderDetailID)
                .ForeignKey("dbo.Orders", t => t.OrderID, cascadeDelete: true)
                .Index(t => t.OrderID);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Wards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DistrictId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Districts", t => t.DistrictId, cascadeDelete: true)
                .Index(t => t.DistrictId);
            
            CreateTable(
                "dbo.Districts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ProvinceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Provinces", t => t.ProvinceId, cascadeDelete: true)
                .Index(t => t.ProvinceId);
            
            CreateTable(
                "dbo.Provinces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        PaymentID = c.Guid(nullable: false),
                        PaymentName = c.String(),
                        PaymentGuide = c.String(),
                        PaymentAvatar = c.String(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PaymentID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Guid(nullable: false),
                        CategoryID = c.Guid(nullable: false),
                        ChildCategoryID = c.Guid(nullable: false),
                        TagName = c.String(),
                    })
                .PrimaryKey(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Shops", "WardID", "dbo.Wards");
            DropForeignKey("dbo.Wards", "DistrictId", "dbo.Districts");
            DropForeignKey("dbo.Districts", "ProvinceId", "dbo.Provinces");
            DropForeignKey("dbo.Shops", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Orders", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderDetails", "OrderID", "dbo.Orders");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Products", "ShopID", "dbo.Shops");
            DropForeignKey("dbo.Commissions", "ShopID", "dbo.Shops");
            DropForeignKey("dbo.ProductPromotions", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductPhotoes", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductColors", "ProductID", "dbo.Products");
            DropForeignKey("dbo.Products", "ChildCategoryID", "dbo.ChildCategories");
            DropForeignKey("dbo.ChildCategories", "CategoryID", "dbo.Categories");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Districts", new[] { "ProvinceId" });
            DropIndex("dbo.Wards", new[] { "DistrictId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.OrderDetails", new[] { "OrderID" });
            DropIndex("dbo.Orders", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Commissions", new[] { "ShopID" });
            DropIndex("dbo.Shops", new[] { "User_Id" });
            DropIndex("dbo.Shops", new[] { "WardID" });
            DropIndex("dbo.ProductPromotions", new[] { "ProductID" });
            DropIndex("dbo.ProductPhotoes", new[] { "ProductID" });
            DropIndex("dbo.ProductColors", new[] { "ProductID" });
            DropIndex("dbo.Products", new[] { "ChildCategoryID" });
            DropIndex("dbo.Products", new[] { "ShopID" });
            DropIndex("dbo.ChildCategories", new[] { "CategoryID" });
            DropTable("dbo.Tags");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Payments");
            DropTable("dbo.Provinces");
            DropTable("dbo.Districts");
            DropTable("dbo.Wards");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Orders");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Commissions");
            DropTable("dbo.Shops");
            DropTable("dbo.ProductPromotions");
            DropTable("dbo.ProductPhotoes");
            DropTable("dbo.ProductColors");
            DropTable("dbo.Products");
            DropTable("dbo.ChildCategories");
            DropTable("dbo.Categories");
        }
    }
}
