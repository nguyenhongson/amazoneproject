namespace Amazon.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addforeignekyProductOrderdetail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "OrderDetail_OrderDetailID", c => c.Guid());
            CreateIndex("dbo.Products", "OrderDetail_OrderDetailID");
            AddForeignKey("dbo.Products", "OrderDetail_OrderDetailID", "dbo.OrderDetails", "OrderDetailID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "OrderDetail_OrderDetailID", "dbo.OrderDetails");
            DropIndex("dbo.Products", new[] { "OrderDetail_OrderDetailID" });
            DropColumn("dbo.Products", "OrderDetail_OrderDetailID");
        }
    }
}
