﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Domain.Models
{
    public class AmazonEnum
    {
        public enum ShopStatus
        {
            Inactive = 0,
            Active = 1
        }
    }
}
