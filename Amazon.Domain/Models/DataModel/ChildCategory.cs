﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Amazon.Models.DataModel
{
    public class ChildCategory
    {
        [Key]
        public Guid ChildCategoryID { get; set; }
        [Required]
        //public Guid Category { get; set; }
        public Guid CategoryID { get; set; }
        public Category Category { get; set; }
        [Required]
        public string ChildCategoryName { get; set; }
        public string ChildCategoryAvatar { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}