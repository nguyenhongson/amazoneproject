﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Amazon.Models.DataModel
{
    public class Commission
    {
        [Key]
        public Guid CommissionID { get; set; }
        [Required]
        public Guid ShopID { get; set; }
        public Shop Shop { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime PayDate { get; set; }
        
        public float TotalMoney { get; set; }
        public int Status { get; set; }

    }
}