﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Amazon.Models.DataModel
{
    public class Payment
    {
        [Key]
        public Guid PaymentID { get; set; }
        public string PaymentName { get; set; }
        public string PaymentGuide { get; set; }
        public string PaymentAvatar { get; set; }
        public int Status { get; set; }
    }
}