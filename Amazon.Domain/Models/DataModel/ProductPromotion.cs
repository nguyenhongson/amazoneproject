﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Amazon.Models.DataModel
{
    public class ProductPromotion
    {
        [Key]
        public Guid ProductPromotionID { get; set; }
        [Required]
        public Guid ProductID { get; set; }
        public Product Product { get; set; }
        public string ContentPromotion { get; set; }
    }
}