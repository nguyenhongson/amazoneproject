﻿using Amazon.Models.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Models.DataModel
{
  public  class ProductProperty
    {
        [Key]
        public Guid ProductPropertyID { get; set; }
        [Required]
        public Guid ProductID { get; set; }
        public Product Product { get; set; }
        public string ProprertyXML { get; set; }
    }
}
