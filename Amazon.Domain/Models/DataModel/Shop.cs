﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using static Amazon.Domain.Models.AmazonEnum;
namespace Amazon.Models.DataModel
{
    public class Shop
    {
        [Key]
        public Guid ShopID { get; set; }
        [Required]
        public string UserID { get; set; }
        public ApplicationUser User { get; set; }

        [MaxLength(300)]
        public string ShopName { get; set; }
        public int WardID { get; set; }
        public Ward Ward { get; set; }
        public ShopStatus Status { get; set; }

        public virtual ICollection<Commission> Commissions { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}