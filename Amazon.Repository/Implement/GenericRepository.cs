﻿using Amazon.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Models;
using Amazon.Domain.Models.DataModel;

namespace Amazon.Repository.Implement
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class/*, IEntity*/
    {
        private ApplicationDbContext _context;
        internal DbSet<TEntity> _dbSet;

        public GenericRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public TEntity Create(TEntity entity)
        {
            var result = _context.Set<TEntity>().Add(entity);
            _context.SaveChanges();
            return result;
        }


        public IEnumerable<TEntity> CreateMany(IEnumerable<TEntity> entities)
        {
            var result =  _context.Set<TEntity>().AddRange(entities);
            _context.SaveChanges();
            return result;
        }

        //public TEntity Delete(Guid id)
        //{
        //    var entity = GetById(id);
        //    var result = _context.Set<TEntity>().Remove(entity);
        //    _context.SaveChanges();
        //    return result;
        //}

        public IQueryable<TEntity> GetAll()
        {
            return _context.Set<TEntity>().AsNoTracking();
        }

        //public virtual TEntity GetById(Guid id)
        //{

        //   // return _context.Set<TEntity>().AsNoTracking().FirstOrDefault(x => x.Id == id);
        //     return _context.Set<TEntity>().SingleOrDefault(x => x.Id == id);
        //}

        //public void IncludeEntity<TInclude>(TEntity entity, string propertyNavition)
        //{
        //    throw new NotImplementedException();
        //}

        public TEntity Update(TEntity entity)
        {
            var result = _context.Set<TEntity>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
            return result;
        }

    }
}
