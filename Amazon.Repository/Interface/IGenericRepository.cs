﻿using Amazon.Domain.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Amazon.Repository.Interface
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        //TEntity GetById(Guid id);
        TEntity Create(TEntity entity);
        IEnumerable<TEntity> CreateMany(IEnumerable<TEntity> entities);
        TEntity Update(TEntity entity);
        //TEntity Delete(Guid id);
       
    }
}
