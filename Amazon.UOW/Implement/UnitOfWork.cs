﻿using Amazon.Models;
using Amazon.Models.DataModel;
using Amazon.Repository.Implement;
using Amazon.Repository.Interface;
using Amazon.UOW.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.UOW.Implement
{
    public class UnitOfWork : IUnitOfWork
    {
       
        private DbContextTransaction _transaction;
        public ApplicationDbContext DbContext { get; set; }

        public IGenericRepository<Category> CategoryRepository { get;  set; }
        public IGenericRepository<ChildCategory> ChildCategoryRepository { get; private set; }
        public IGenericRepository<Commission> CommissionRepository { get; private set; }
        public  IGenericRepository<District> DistrictRepository { get; private set; }
        public IGenericRepository<Order> OrderRepository { get; private set; }
        public IGenericRepository<OrderDetail> OrderDetailRepository { get; private set; }
        public IGenericRepository<Product> ProductRepository { get; private set; }
        public IGenericRepository<ProductPhoto> ProductPhotoRepository { get; private set; }
        public IGenericRepository<ProductPromotion> ProductPromotionRepository { get; private set; }
        public IGenericRepository<ProductProperty> ProductPropertyRepository { get; private set; }
        public IGenericRepository<Province> ProvinceRepository { get; private set; }
        public IGenericRepository<Shop> ShopRepository { get; private set; }
        public IGenericRepository<Tag> TagRepository { get; private set; }
        public IGenericRepository<Ward> WardRepository { get; private set; }


        //public UnitOfWork(ApplicationDbContext dbContext)
        //{
        //    DbContext = dbContext;
        //    AttributeHouseRepository = new GenericRepository<AttributeHouse>(DbContext);
        //    HouseRepository = new GenericRepository<House>(DbContext);
        //    HouseAttributeHouseRepository = new GenericRepository<HouseAttributeHouse>(DbContext);
        //    ImageRepository = new GenericRepository<Image>(DbContext);
        //    PostRepository = new GenericRepository<Post>(DbContext);
        //    ValueAttributeRepository = new GenericRepository<ValueAttribute>(DbContext);
        //    RentalDetailRepository = new GenericRepository<RentalDetail>(DbContext);
        //}
        //public UnitOfWork(ApplicationDbContext dbContext)
        //{
        //    DbContext = dbContext;
        //    CategoryRepository = new GenericRepository<Category>(DbContext);

        //   // ChildCategoryRepository = new GenericRepository<ChildCategory>(DbContext);

        //}
        public UnitOfWork(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
            CategoryRepository = new GenericRepository<Category>(DbContext);
            ChildCategoryRepository=new GenericRepository<ChildCategory>(DbContext);
            CommissionRepository = new GenericRepository<Commission>(DbContext);
            OrderRepository = new GenericRepository<Order>(DbContext);
            OrderDetailRepository = new GenericRepository<OrderDetail>(DbContext);
            ProductRepository = new GenericRepository<Product>(DbContext);
            ProductPhotoRepository = new GenericRepository<ProductPhoto>(DbContext);
            ProductPromotionRepository = new GenericRepository<ProductPromotion>(DbContext);
            ProductPropertyRepository = new GenericRepository<ProductProperty>(DbContext);
            ShopRepository = new GenericRepository<Shop>(DbContext);
            TagRepository = new GenericRepository<Tag>(DbContext);
        }


        public DbContextTransaction BeginTransaction()
        {
            _transaction = DbContext.Database.BeginTransaction();
            return _transaction;
        }

        public int Commit()
        {
            try
            {
                // commit transaction if there is one active
                if (_transaction != null)
                    _transaction.Commit();
            }
            catch (Exception)
            {

                // rollback if there was an exception
                if (_transaction != null)
                    _transaction.Rollback();
                throw;
            }
            return DbContext.SaveChanges();
        }

        public void RollBack()
        {
            try
            {
                if (_transaction != null)
                    _transaction.Rollback();
            }
            catch
            {
                throw;
            }
        }
        public void Dispose()
        {
            DbContext.Dispose();
        }

        public async Task SaveAsync()
        {
            await DbContext.SaveChangesAsync();
        }
    }
}
