﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Amazon.AppService.Interface;
using Amazon.Repository.Implement;
using Amazon.Models.DataModel;
using Amazon.Models;
using System.Runtime;
using Amazon.Domain.Models;

namespace Amazon.Areas.Admin.Controllers
{
    public class HomeAdminController :Controller
    {
        private ApplicationDbContext DbContext =new ApplicationDbContext();
       
        private readonly ICategoryService categoryService;

        //loi o day chua fix

        //public HomeAdminController(ICategoryService categoryService)
        //{
        //    this.categoryService = categoryService;
        //}
        public ActionResult LoginAdmin()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LoginAdmin(string uname, string psw)
        {
            if (ModelState.IsValid)
            {
                //  var hash = GenerateHash(psw, Settings.Default.salt);
               // var user = from a in DbContext.Users where a.UserName == uname && a.PasswordHash == GenerateHash(psw);
               if(uname== DbContext.Users.Select(n=>n.UserName).FirstOrDefault()&& psw == "Admin12@")
                {
                    Session["UserID"] = DbContext.Users.Select(n => n.Id).FirstOrDefault();
                    return RedirectToAction("Index","HomeAdmin", new { area = "Admin" });
                }
                else
                {
                    TempData["alertLogin"] = "Login failed, please check username and password";
                    return RedirectToAction("LoginAdmin", "Admin/HomeAdmin");
                }
            }
            return View();
        }

        private static string GenerateHash(string value, string salt)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes(salt + value);
            data = System.Security.Cryptography.MD5.Create().ComputeHash(data);
            return Convert.ToBase64String(data);
        }



        // GET: Admin/Home
        [Authorize(Roles = RoleName.Admin)]
        public ActionResult Index()
        {
            //if (Session["UserID"] == null)
            //{
            //    return RedirectToAction("LoginAdmin","HomeAdmin");
            //}
            //List<Category> listCategory = new List<Category>();
            //listCategory = categoryService.GetAllCategory();
            return View();
        }
        //[Authorize]
        [Authorize(Roles = "Admin")]
        public ActionResult Category(ICategoryService categoryService)
        {
            //var listCategory = _iCategoryService.GetAllCategory();
            
           var n= categoryService.GetAllCategory();

            return View();
        }
        [Authorize(Roles = RoleName.Admin)]
        public List<Category> GetAllCategory()
        {
            List<Category> listCategory = new List<Category>();
            listCategory = categoryService.GetAllCategory();
            return listCategory;

        }
    }
}