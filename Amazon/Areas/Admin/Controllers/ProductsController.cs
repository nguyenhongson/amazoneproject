﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Amazon.AppService.Interface;
using Amazon.AppService.ViewModel;
using Amazon.Models;
using Amazon.Models.DataModel;
using static Amazon.AppService.Implement.ProductPropertyService;

namespace Amazon.Areas.Admin.Controllers
{
    public class ProductsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public readonly IProductService _productService;
        public readonly IProductPhotoService _productPhotoService;
        public readonly IProductPropertyService _productPropertyService;
        public ProductsController(IProductService productService, IProductPhotoService productPhotoService, IProductPropertyService productPropertyService)
        {
            _productService = productService;
            _productPhotoService = productPhotoService;
            _productPropertyService = productPropertyService;
        }
       
        public ActionResult Index()
        {
            //var product = db.Product.Include(p => p.ChildCategory).Include(p => p.Shop);
            //return View(product.ToList());
            return View(_productService.GetListProduct());
        }

        public ActionResult Details(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           ProductViewModel oneProductInformation= _productService.GetProduct(id);
            if (oneProductInformation == null)
            {
                return HttpNotFound();
            }
            return View(oneProductInformation);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.Categoryviewbag1 = new SelectList(db.Category, "CategoryID", "CategoryName");
            ViewBag.Categoryviewbag = db.Category.ToList();
          //  ViewBag.ChildCategoryID = new SelectList(db.ChildCategory, "ChildCategoryID", "ChildCategoryName");
            ViewBag.ShopID = new SelectList(db.Shop, "ShopID", "ShopName");
            return View();
        }
        public JsonResult GetChildCategoryByIdCategory(Guid IdCategory)
        {
            return Json(db.ChildCategory.Where(n=>n.CategoryID== IdCategory).
                Select(s=>new { IdCate=s.ChildCategoryID,NameCate=s.ChildCategoryName }), JsonRequestBehavior.AllowGet);
        }
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProduct(FormCollection fc, HttpPostedFileBase fileImageProduct, HttpPostedFileBase[] files)
        {
            if (fc["_idChildCategory"].ToString()=="-1")
            {
              TempData["messageErrorIdChildCate"] = "You must select Category and child Category";
                return RedirectToAction("Create");
            }
            if (ModelState.IsValid)
            {
                ProductViewModel oneProduct = new ProductViewModel();
                oneProduct.ProductID = Guid.NewGuid();

                //save temp guidProduct
                  Guid  TempGuidProduct = oneProduct.ProductID;

                //shopid
                Guid ShopId = Guid.Parse(fc["ShopId"]);
                //CHECK shopid have or not in db

                oneProduct.ShopID = ShopId;
                Guid idChicategory = Guid.Parse(fc["_idChildCategory"]);
                oneProduct.ChildCategoryID = idChicategory;
                oneProduct.Status = 1;
                oneProduct.ProductName = fc["productName"];
                string quantitynumber = fc["Quantity"];
                oneProduct.Quantity = Int32.Parse(quantitynumber);
                oneProduct.MinOrder = Int32.Parse(fc["MinOrder"]);

                oneProduct.Price = Int32.Parse(fc["Price"]);
                oneProduct.Description = fc["contentDescription"];

                //call service add product
                //add propety of product

                //saveFile
                string path = "";
                if (fileImageProduct != null && fileImageProduct.ContentLength > 0)
                    try
                    {
                        path = Path.Combine(Server.MapPath("~/Content/LocalImage"),
                                                   Path.GetFileName(fileImageProduct.FileName));
                        fileImageProduct.SaveAs(path);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    }
                //save link path of product
              //  oneProduct.ProductAvatar = path;

                oneProduct.ProductAvatar= Path.GetFileName(fileImageProduct.FileName);


                //Save product without property
                if (fc["numberofproperty"] == "0")
                {
                    //gửi model sang service để lưu vào db
                    _productService.CreateProduct(oneProduct);
                }
                //save product with many property
                else
                {
                    List<PropertyOfProductXML> listPro = new List<PropertyOfProductXML>();
                 
                    int numberProperty = Int32.Parse(fc["numberofproperty"]);
                    for(int n=1;n<= numberProperty*2; n++)
                    {
                        PropertyOfProductXML oneProperty = new PropertyOfProductXML();
                        if (n % 2 == 1)
                        {
                            oneProperty.Attribute = (fc["kiki" + n]).ToString();
                            string numbertwo = (n + 1).ToString();
                            oneProperty.Value= (fc["kiki" + numbertwo]).ToString();
                            listPro.Add(oneProperty);
                        }
                    }
                    _productService.CreateProductWithManyProperty(oneProduct, listPro);
                }
                //save product image detail
                foreach (HttpPostedFileBase file in files)
                {
                    //Checking file is available to save.  
                    if (file != null && file != fileImageProduct && file.ContentLength > 0)
                    {
                        var InputFileName = Path.GetFileName(file.FileName);
                        var ServerSavePath = Path.Combine(Server.MapPath("~/Content/LocalImage"), InputFileName);
                        file.SaveAs(ServerSavePath);

                        //path = Path.Combine(Server.MapPath("~/Content/LocalImage"),
                        //                          Path.GetFileName(fileImageProduct.FileName));
                        //fileImageProduct.SaveAs(path);
                    }
                    //call productphoto service save data
                    ProductPhoto OnePhoto = new ProductPhoto();
                    OnePhoto.ProductID = TempGuidProduct;
                    OnePhoto.PoductPhotoID = Guid.NewGuid();
                    OnePhoto.PhotoPath = Path.GetFileName(file.FileName);
                    OnePhoto.Status = 1;
                    _productPhotoService.CreateProductPhoto(OnePhoto);
                }
            }
            return RedirectToAction("Index","Products");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductID,ProductName,ShopID,Quantity,ProductAvatar,Description,Status,MinOrder,Price,ChildCategoryID")] Product product)
        {
            if (ModelState.IsValid)
            {
                product.ProductID = Guid.NewGuid();
                db.Product.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ChildCategoryID = new SelectList(db.ChildCategory, "ChildCategoryID", "ChildCategoryName", product.ChildCategoryID);
            ViewBag.ShopID = new SelectList(db.Shop, "ShopID", "ShopName", product.ShopID);
            return View(product);
        }

        public ActionResult Edit(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Product product = db.Product.Find(id);
            //if (product == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(product);

            //  ViewBag.ChildCategoryID = new SelectList(db.ChildCategory, "ChildCategoryID", "ChildCategoryName", product.ChildCategoryID);
            //    ViewBag.ShopID = new SelectList(db.Shop, "ShopID", "ShopName", product.ShopID);

            ViewBag.Categoryviewbag = db.Category.ToList();

            //get one product and many image, property show with productViewModel---ProductViewModelList
            ProductViewModelList listInfor = new ProductViewModelList();
            ProductViewModel oneProductViewModel= _productService.GetProduct(id);

            if (oneProductViewModel == null)
            {
                return HttpNotFound();
            }
            listInfor.oneProduct = oneProductViewModel;


            //get many property of product
            List<SerializableProductProperty> listproper= _productPropertyService.GetlistPropertyByIdProduct(id);
            listInfor.listProperty = listproper;

            ViewBag.numberofProperty= listproper.Count();
            return View(listInfor);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit( ProductViewModel productViewModel, FormCollection fc, HttpPostedFileBase fileImageProduct)
        {
            //if (ModelState.IsValid)
            //{
            //    db.Entry(product).State = EntityState.Modified;
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            //ViewBag.ChildCategoryID = new SelectList(db.ChildCategory, "ChildCategoryID", "ChildCategoryName", product.ChildCategoryID);
            //ViewBag.ShopID = new SelectList(db.Shop, "ShopID", "ShopName", product.ShopID);
            //return View(product);
            
            Product oneProduct = new Product();
           // oneProduct.ProductID=
           // _productService.UpdateProduct();

            //file ok, productviewmodel nullm fc ok



            return View();
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Product product = db.Product.Find(id);
            db.Product.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
