﻿using System;
using System.Web;
using System.Web.Mvc;
using Amazon.Models.DataModel;
using Amazon.AppService.Interface;
using Amazon.UOW.Interface;
using Microsoft.AspNet.Identity.Owin;
using System.Net;
using Microsoft.AspNet.Identity;
using Amazon.Models;
using Amazon.Domain.Models;
using static Amazon.Domain.Models.AmazonEnum;
using System.Linq;
using System.Collections.Generic;
using PagedList;

namespace Amazon.Areas.Admin.Controllers
{
    public class ShopController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private IShopService _shopService;
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: ShopManager/ShopManager
        public ShopController(IShopService shopService)
        {
            _shopService = shopService;
        }
        [Authorize(Roles = RoleName.Admin)]
        public ActionResult Index()
        {
            var shop = _shopService.GetAll();
            return View(shop);
        }

        [Authorize(Roles = RoleName.Admin)]
        public ActionResult SaveShop(Shop shop)
        {
            ViewBag.WardID = new SelectList(db.Ward, "Id", "Name", shop.WardID);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveShop(ShopRegisterViewModel shop)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = shop.Email, Email = shop.Email };
                var result = UserManager.Create(user, shop.Password);
                if (result.Succeeded)
                {

                    UserManager.AddToRoles(user.Id, "Shop");
                    _shopService.CreateShop(new Shop
                    {
                        ShopID = Guid.NewGuid(),
                        UserID = user.Id,
                        ShopName = shop.ShopName,
                        WardID = shop.WardID,
                        Status = ShopStatus.Active

                    });
                    return RedirectToAction("Index", "Shop");
                }
            }
            ViewBag.WardID = new SelectList(db.Ward, "Id", "Name", shop.WardID);
            return View();
        }

        
        [Authorize(Roles = RoleName.Admin)]
        [HttpPost]
        public ActionResult ShopDetail(Shop shop)
        {
            //ViewBag.Status = new SelectList(db.Shop,"StatusID");
            ViewBag.WardID = new SelectList(db.Ward, "Id", "Name", shop.WardID);
            _shopService.UpdateShop(shop);
            return RedirectToAction("Index", "Shop");
        }
        public ActionResult ShopDetail(Guid id)
        {
            //var array = Enum.GetValues(typeof(SomeEnum)).Cast<SomeEnum>().ToArray();
            var array = Enum.GetValues(typeof(ShopStatus)).Cast<ShopStatus>().ToArray();
            Shop model = db.Shop.Find(id);
            ViewBag.Status = new SelectList(array);
            ViewBag.WardID = new SelectList(db.Ward, "Id", "Name", model.WardID);
            return View(model);
        }

        [Authorize(Roles = RoleName.Admin)]
        
        public ActionResult ShopDelete(Guid id)
        {
            _shopService.DeleteShop(id);
            return RedirectToAction("Index","Shop");
        }
        public ActionResult PagedShop(int? page)
        {
            if (page == null) page = 1;
            var links = (from l in db.Shop
                         select l).OrderBy(x => x.ShopID);
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(links.ToPagedList(pageNumber, pageSize));
        }
        
    }
}