﻿using Amazon.AppService.Interface;
using Amazon.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Amazon.Areas.ShopManager.Controllers
{
    public class ShopManagerController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private IShopService _shopService;
        public ShopManagerController(IShopService shopService)
        {
            _shopService = shopService;
        }
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: ShopManager/ShopManager
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListProduct()
        {
            var user = UserManager.FindByName(User.Identity.Name);
            var shopID = (from s in db.Shop
                          where s.UserID == user.Id
                          select s.ShopID).FirstOrDefault();
            return View(_shopService.GetListProductByShopID(shopID));
        }
    }
}