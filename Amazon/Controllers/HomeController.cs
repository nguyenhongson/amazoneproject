﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;
using Amazon.AppService.Interface;
using Amazon.AppService.ViewModel;
using Amazon.Models;
using Amazon.Models.DataModel;

namespace Amazon.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public readonly ICategoryService _categoryService;
        public readonly IProductService _productService;
        public readonly IChildCategoryService _childCategoryService;
        public readonly ISearchService _searchService;
        public HomeController( IProductService productService, ICategoryService categoryService, IChildCategoryService childCategoryService, ISearchService searchService)
        {
            _productService = productService;
            _categoryService = categoryService;
            _childCategoryService = childCategoryService;
            _searchService = searchService;
        }
        public ActionResult Index()
        {
            ViewBag.ChildCategoryID = new SelectList(db.ChildCategory, "ChildCategoryID", "ChildCategoryName");
            
            return View(_productService.GetListProduct());
        }
        public ActionResult About(HttpPostedFileBase file)
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }
        public ActionResult Contact()
        {
           
            return View();
        }
        [HttpPost]
        public ActionResult Search(string searchString, Guid childCategoryID)
        {
            ViewBag.ChildCategoryID = new SelectList(db.ChildCategory, "ChildCategoryID", "ChildCategoryName");
            var listProduct = _searchService.SearchProductByCC(searchString, childCategoryID);
            return View(listProduct);
        }
    }
}