﻿using Amazon.AppService.Interface;
using Amazon.AppService.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Amazon.AppService.Implement.ProductPropertyService;

namespace Amazon.Controllers
{
    public class ProductController : Controller
    {
        public IProductService _productService;
        public IProductPropertyService _productPropertyService;
        private readonly IProductPhotoService _productPhotoService;
        public ProductController(IProductService productService,IProductPropertyService productPropertyService, IProductPhotoService productPhotoService)
        {
            _productService = productService;
            _productPropertyService = productPropertyService;
            _productPhotoService = productPhotoService;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ViewProduct(Guid id)
        {
            ProductViewModelList listInforOfOneProduct = new ProductViewModelList();
            if (id != null)
            {
                //return infor product
              //  ProductViewModel oneProductModel = new ProductViewModel();

                listInforOfOneProduct.oneProduct = _productService.GetProduct(id);
                //return list image of product

                //return list Property
               
                listInforOfOneProduct.listProperty = _productPropertyService.GetlistPropertyByIdProduct(id);



                listInforOfOneProduct.listPhoto= _productPhotoService.GetListPhotoByIdProduct(id);
            }
            return View(listInforOfOneProduct);
        }
    }
}