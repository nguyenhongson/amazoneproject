using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Amazon.AppService.Mapping;
using AutoMapper;
using Autofac;
using Autofac.Integration.Mvc;
using Amazon.AppService.Implement;
using Amazon.AppService.Interface;
using Amazon.UOW.Implement;
using Amazon.UOW.Interface;
using Amazon.Models;

namespace Amazon
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutofacRegister();

            //mapping
           // Mapper.Initialize(cfg => cfg.AddProfile<ModelMapping>());
        }
        private void AutofacRegister()
        {
            var builder = new ContainerBuilder();
            // Register all controllers in the Mvc Application assembly
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<CategoryService>().As<ICategoryService>();
            builder.RegisterType<ChildCategoryService>().As<IChildCategoryService>();
            builder.RegisterType<ProductService>().As<IProductService>();
            builder.RegisterType<CommissionService>().As<ICommission>();
            builder.RegisterType<OrderService>().As<IOderService>();
            builder.RegisterType<PaymentService>().As<IPaymentService>();
            builder.RegisterType<ProductPhotoService>().As<IProductPhotoService>();
            builder.RegisterType<ProductPromotionService>().As<IProductPromotion>();
            builder.RegisterType<ProductPropertyService>().As<IProductPropertyService>();
            builder.RegisterType<ShopService>().As<IShopService>();
            builder.RegisterType<TagService>().As<ITagService>();
            builder.RegisterType<SearchService>().As<ISearchService>();

            //builder.RegisterType<HouseAttributeHouseService>().As<IHouseAttributeHouseService>();
            //builder.RegisterType<ImageService>().As<IImageService>();
            builder.RegisterType<ApplicationDbContext>();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();


            builder.RegisterFilterProvider();
            // Registered Generic Repository Service
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
